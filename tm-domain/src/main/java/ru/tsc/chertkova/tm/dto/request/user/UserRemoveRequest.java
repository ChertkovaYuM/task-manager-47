package ru.tsc.chertkova.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public class UserRemoveRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserRemoveRequest(@Nullable String token, @Nullable String login) {
        super(token);
        this.login = login;
    }

}
