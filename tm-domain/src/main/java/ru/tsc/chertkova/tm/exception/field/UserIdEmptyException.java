package ru.tsc.chertkova.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User ID is empty...");
    }

}
