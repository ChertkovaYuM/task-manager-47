package ru.tsc.chertkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.model.AbstractModelDTO;

import java.util.List;

public interface IAbstractRepositoryDTO<M extends AbstractModelDTO> {

    void add(@NotNull M model);

    void clear();

    @NotNull
    List<M> findAll();

    @Nullable
    M findById(@NotNull String id);

    int getSize();

    void removeById(@NotNull String id);

    void update(@NotNull M model);

}
