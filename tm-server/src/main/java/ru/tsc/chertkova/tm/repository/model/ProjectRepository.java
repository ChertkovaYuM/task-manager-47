package ru.tsc.chertkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.model.IProjectRepository;
import ru.tsc.chertkova.tm.model.Project;


import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnerModelRepository<Project>
        implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final Project model) {
        super.add(model);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Project").executeUpdate();
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("FROM Project", Project.class).getResultList();
    }

    @Nullable
    @Override
    public Project findById(@NotNull final String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Project e", Integer.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(Project.class, id));
    }

    @Override
    public void update(@NotNull Project model) {
        entityManager.createQuery("UPDATE Project e SET e.name=:name AND e.description=:description WHERE e.user.id=:userId AND e.id=:id")
                .setParameter("name", model.getName())
                .setParameter("description", model.getDescription())
                .setParameter("id", model.getId())
                .setParameter("userId", model.getUser().getId())
                .executeUpdate();
    }

    @Override
    public void add(@NotNull final String userId,
                    @NotNull final Project model) {
        add(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Project e WHERE e.user.id=:userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id=:userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findById(@NotNull final String userId,
                            @NotNull final String id) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id=:userId AND e.id=:id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Project e WHERE e.user.id=:userId", Integer.class)
                .setParameter("userId", userId)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId,
                           @NotNull final String id) {
        entityManager.remove(entityManager.getReference(Project.class, id));
    }

    @Override
    public void update(@NotNull final String userId,
                       @NotNull final Project model) {
        update(model);
    }

    @Override
    public void changeStatus(@NotNull final String id,
                             @NotNull final String userId,
                             @NotNull final String status) {
        entityManager.createQuery("UPDATE Project e SET e.status=:status WHERE e.user.id=:userId AND e.id=:id")
                .setParameter("status", status)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public int existsById(@Nullable final String id) {
        return entityManager
                .createQuery("SELECT e FROM Project e WHERE e.id=:id", Integer.class)
                .setParameter("id", id)
                .setMaxResults(1).getSingleResult();

    }

}
