package ru.tsc.chertkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.model.ITaskRepository;
import ru.tsc.chertkova.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository
        extends AbstractUserOwnerModelRepository<Task>
        implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull Task model) {
        super.add(model);
    }

    @Override
    @Nullable
    public List<Task> findAllByProjectId(@Nullable final String userId,
                                         @Nullable final String projectId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id=:userId AND e.project.id=:projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void changeStatus(@NotNull final String id,
                             @NotNull final String userId,
                             @NotNull final String status) {
        entityManager.createQuery("UPDATE Task e SET e.status=:status WHERE e.user.id=:userId AND e.id=:id")
                .setParameter("status", status)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public int existsById(@Nullable final String id) {
        return entityManager
                .createQuery("SELECT e FROM Task e WHERE e.id=:id", Integer.class)
                .setParameter("id", id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void bindTaskToProject(@NotNull final String taskId,
                                  @NotNull final String projectId,
                                  @NotNull final String userId) {
        entityManager.createQuery("UPDATE Task e SET e.project.id=:projectId WHERE e.user.id=:userId AND e.id=:taskId")
                .setParameter("taskId", taskId)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Task").executeUpdate();
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        return entityManager.createQuery("FROM Task", Task.class).getResultList();
    }

    @Override
    @Nullable
    public Task findById(@NotNull final String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Task e", Integer.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(Task.class, id));
    }

    @Override
    public void update(@NotNull final Task model) {
        entityManager.createQuery("UPDATE Task e SET e.name=:name AND e.description=:description WHERE e.user.id=:userId AND e.id=:id")
                .setParameter("name", model.getName())
                .setParameter("description", model.getDescription())
                .setParameter("id", model.getId())
                .setParameter("userId", model.getUser().getId())
                .executeUpdate();
    }

    @Override
    public void add(@NotNull final String userId,
                    @NotNull final Task model) {
        add(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Task e WHERE e.user.id=:userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id=:userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public Task findById(@NotNull final String userId,
                         @NotNull final String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Task e WHERE e.user.id=:userId", Integer.class)
                .setParameter("userId", userId)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId,
                           @NotNull final String id) {
        entityManager.remove(entityManager.getReference(Task.class, id));
    }

    @Override
    public void update(@NotNull final String userId,
                       @NotNull final Task model) {
        update(model);
    }

}
