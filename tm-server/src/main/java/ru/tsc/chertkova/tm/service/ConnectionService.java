package ru.tsc.chertkova.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.dto.model.ProjectDTO;
import ru.tsc.chertkova.tm.dto.model.TaskDTO;
import ru.tsc.chertkova.tm.dto.model.UserDTO;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        entityManagerFactory = factory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    @Override
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(Environment.URL, propertyService.getDatabaseUrl());
        settings.put(Environment.USER, propertyService.getDatabaseUsername());
        settings.put(Environment.PASS, propertyService.getDatabasePassword());
        settings.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHBM2DDL());
        settings.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSQL());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseUseSecondLvlCache());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDatabaseUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDatabaseUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDatabaseCacheRegionPrefix());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDatabaseCacheFactoryClass());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDatabaseCacheConfigFile());

        @NotNull final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
