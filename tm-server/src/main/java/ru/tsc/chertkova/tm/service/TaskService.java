package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.model.ITaskRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.ITaskService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.*;
import ru.tsc.chertkova.tm.exception.field.DescriptionEmptyException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnerService<Task> implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService, @Nullable IUserService userService) {
        super(connectionService, userService);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId,
                                         @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable List<Task> tasks;
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            tasks = taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    @Nullable
    public Task add(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(task.getUser().getId()).orElseThrow(UserNotFoundException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            Optional.ofNullable(task.getName()).orElseThrow(NameEmptyException::new);
            Optional.ofNullable(task.getProjectId()).orElseThrow(TaskNotFoundException::new);
            Optional.ofNullable(task.getUser().getId()).orElseThrow(TaskNotFoundException::new);
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task updateById(@Nullable final String userId,
                           @Nullable final String id,
                           @Nullable final String name,
                           @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable Task task;
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            task = findById(userId, id);
            Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            task = findById(userId, id);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(@Nullable final String userId,
                                     @Nullable final String id,
                                     @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable Task task;
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            task = findById(userId, id);
            Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
            taskRepository.changeStatus(id, userId, status.getDisplayName());
            entityManager.getTransaction().commit();
            task = findById(userId, id);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull int count = taskRepository.existsById(id);
        return count > 0;
    }

    @Override
    public @Nullable Task findById(@Nullable final String userId,
                                   @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable Task task;
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            task = taskRepository.findById(userId, id);
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public Task removeById(@Nullable final String userId,
                           @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final Task task;
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            task = findById(userId, id);
            taskRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public Task remove(@Nullable final String userId,
                       @Nullable final Task task) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(findById(task.getUser().getId(), task.getId())).orElseThrow(TaskNotFoundException::new);
        removeById(task.getUser().getId(), task.getId());
        return task;
    }

    @Override
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        int size = 0;
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            size = taskRepository.getSize(userId);
        } finally {
            entityManager.close();
        }
        return size;
    }

    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable List<Task> tasks;
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            tasks = taskRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    @Nullable
    public List<Task> addAll(@NotNull final List<Task> tasks) {
        Optional.ofNullable(tasks).orElseThrow(ProjectNotFoundException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            for (Task t : tasks) {
                taskRepository.add(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    @Nullable
    public List<Task> removeAll(@Nullable final List<Task> tasks) {
        Optional.ofNullable(tasks).orElseThrow(ProjectNotFoundException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            for (Task t : tasks) {
                taskRepository.removeById(t.getUser().getId(), t.getId());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return tasks;
    }

}
