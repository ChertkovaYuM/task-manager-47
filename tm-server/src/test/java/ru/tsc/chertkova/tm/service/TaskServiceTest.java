package ru.tsc.chertkova.tm.service;

import org.junit.experimental.categories.Category;
import ru.tsc.chertkova.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class TaskServiceTest {
//
//    @NotNull
//    private final IPropertyService propertyService = new PropertyService();
//
//    @NotNull
//    private final IConnectionService connectionService = new ConnectionService(propertyService);
//
//    @NotNull
//    private final ITaskRepository repository = new TaskRepository(connectionService.getConnection());
//
//    @NotNull
//    private final ITaskService service = new TaskService(connectionService);
//
//    @Rule
//    @NotNull
//    public final ExpectedException thrown = ExpectedException.none();
//
//    @Before
//    public void setUp() {
//        for (Task t :
//                TASK_LIST) {
//            repository.add(t);
//        }
//    }
//
//    @After
//    public void tearDown() {
//        repository.clear();
//    }
//
//    @Test
//    public void findAll() {
//        Assert.assertEquals(TASK_LIST, service.findAll());
//    }
//
//    @Test
//    public void FindAllByUserId() {
//        Assert.assertEquals(ADMIN1_TASK_LIST, service.findAll(ADMIN1.getId()));
//        thrown.expect(UserIdEmptyException.class);
//        service.findAll();
//    }
//
//    @Test
//    public void clear() {
//        service.clear();
//        Assert.assertTrue(repository.findAll().isEmpty());
//    }
//
//    @Test
//    public void clearByUserId() {
//        service.clear(USER1.getId());
//        Assert.assertTrue(repository.findAll(USER1.getId()).isEmpty());
//        thrown.expect(UserIdEmptyException.class);
//        service.clear(null);
//    }
//
//    @Test
//    public void remove() {
//        @NotNull final List<Task> list = new ArrayList<>(repository.findAll());
//        //@Nullable final Task removed = service.remove(USER1_TASK2);
//        //Assert.assertEquals(USER1_TASK2, removed);
//        list.remove(USER1_TASK2);
//        Assert.assertEquals(list, repository.findAll());
//        //Assert.assertNull(service.remove(null));
//        //Assert.assertNull(service.remove(USER1_TASK2));
//    }
//
//    @Test
//    public void removeByUserId() {
//        @NotNull final List<Task> list = new ArrayList<>(repository.findAll());
//        service.remove(USER1.getId(), USER1_TASK2);
//        list.remove(USER1_TASK2);
//        Assert.assertEquals(list, repository.findAll());
//        service.remove(USER1.getId(), null);
//        Assert.assertEquals(list, repository.findAll());
//        service.remove(USER1.getId(), USER2_TASK1);
//        Assert.assertEquals(list, repository.findAll());
//        thrown.expect(UserIdEmptyException.class);
//        service.remove(null, USER2_TASK1);
//        Assert.assertEquals(list, repository.findAll());
//    }
//
//    @Test
//    public void createByNameAndUserId() {
//        repository.clear();
//        @NotNull final Task expected = new Task();
//        expected.setName(USER1_TASK1.getName());
//        expected.setUserId(USER1.getId());
//        service.create(USER1.getId(), USER1_TASK1.getName(), "vot tak vot","1");
//        @NotNull final Task created = repository.findAll().get(0);
//        assertThat(created).isEqualToIgnoringGivenFields(expected, "id");
//        thrown.expect(UserIdEmptyException.class);
//        service.create(null, USER1_TASK1.getName(), "vot tak vot","1");
//        thrown.expect(NameEmptyException.class);
//        service.create(USER1.getId(), null, "vot tak vot","1");
//    }
//
//    @Test
//    public void createByNameAndUserIdAndDescription() {
//        repository.clear();
//        @NotNull final Task expected = new Task();
//        expected.setName(USER1_TASK1.getName());
//        expected.setUserId(USER1.getId());
//        expected.setDescription(USER1_TASK1.getDescription());
//        service.create(USER1.getId(), USER1_TASK1.getName(), USER1_TASK1.getDescription(),"1");
//        @NotNull final Task created = repository.findAll().get(0);
//        assertThat(created).isEqualToIgnoringGivenFields(expected, "id");
//        thrown.expect(UserIdEmptyException.class);
//        service.create(null, USER1_TASK1.getName(), USER1_TASK1.getDescription(),"1");
//        thrown.expect(NameEmptyException.class);
//        service.create(USER1.getId(), null, USER1_TASK1.getDescription(),"1");
//        thrown.expect(DescriptionEmptyException.class);
//        service.create(USER1.getId(), USER1_TASK1.getName(), null,"1");
//    }
//
//    @Test
//    public void addByUserId() {
//        repository.clear();
//        service.add(USER1.getId(), USER1_TASK1);
//        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
//        service.add(USER1.getId(), null);
//        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
//        thrown.expect(UserIdEmptyException.class);
//        service.add(null, USER1_TASK1);
//    }
//
//    @Test
//    public void findById() {
//        Assert.assertEquals(
//                service.findById(USER1.getId(), USER1_TASK2.getId()),
//                USER1_TASK2
//        );
//        Assert.assertNull(repository.findById(USER1.getId(), ADMIN1_TASK2.getId()));
//        thrown.expect(UserIdEmptyException.class);
//        service.findById(null, USER1_TASK1.getId());
//        thrown.expect(IdEmptyException.class);
//        service.findById(USER1.getId(), null);
//    }
//
//    @Test
//    public void removeById() {
//        @NotNull final List<Task> expected = new ArrayList<>(repository.findAll());
//        Assert.assertEquals(
//                service.removeById(USER1.getId(), USER1_TASK2.getId()),
//                USER1_TASK2
//        );
//        expected.remove(USER1_TASK2);
//        Assert.assertEquals(expected, repository.findAll());
//        thrown.expect(UserIdEmptyException.class);
//        service.removeById(null, USER1_TASK1.getId());
//        thrown.expect(IdEmptyException.class);
//        service.removeById(USER1.getId(), null);
//    }
//
//    @Test
//    public void updateById() {
//        @NotNull final Task actual = new Task();
//        actual.setUserId(ADMIN1.getId());
//        repository.add(actual);
//        service.updateById(
//                ADMIN1.getId(),
//                actual.getId(),
//                ADMIN1_TASK1.getName(),
//                ADMIN1_TASK1.getDescription()
//        );
//        assertThat(actual).isEqualToIgnoringGivenFields(ADMIN1_TASK1, "id");
//        thrown.expect(UserIdEmptyException.class);
//        service.updateById(null,
//                actual.getId(),
//                ADMIN1_TASK1.getName(),
//                ADMIN1_TASK1.getDescription()
//        );
//        thrown.expect(IdEmptyException.class);
//        service.updateById(ADMIN1.getId(),
//                null,
//                ADMIN1_TASK1.getName(),
//                ADMIN1_TASK1.getDescription()
//        );
//        thrown.expect(NameEmptyException.class);
//        service.updateById(ADMIN1.getId(),
//                actual.getId(),
//                null,
//                ADMIN1_TASK1.getDescription()
//        );
//    }

}
